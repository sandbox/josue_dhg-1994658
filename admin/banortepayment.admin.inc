<?php

/**
 * Admin configuration for payment with paywords of banorte mexico
 *
 * @author Josue David Hernandez Gutierrez <megadavidtor@gmail.com>
 * @license GPL
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 */

/**
 * payment_configuration() return the form for the configuration of payment
 */
function payment_configuration(){
    $form['mode'] = array(
        '#type'=>'select',
        '#title' => t('Mode'),
        '#options' => array(
            'P'=>t('Production'),
            'Y'=>t('Test (Positive)'),
            'N'=>t('Test (Negative)'),
            'R'=>t('Test (Random)'),
        ),
        '#required' => TRUE,
        '#default_value'=>variable_get('mode_banorte_payment','')
    );
    $form['iscorrect'] = array(
        '#type'=>'textfield',
        '#title' => t('URL to redirect if the transaction is successful'),
        '#required' => TRUE,
        '#default_value'=>variable_get('iscorrect_banorte_payment','')
    );
    $form['isntcorrect'] = array(
        '#type'=>'textfield',
        '#title' => t("URL to redirect if the transaction isn't successful"),
        '#required' => TRUE,
        '#default_value'=>variable_get('isntcorrect_banorte_payment','')
    );
    $form['currency'] = array(
        '#type'=>'checkbox',
        '#title' => t("Currency USD"),
        '#default_value'=>variable_get('currency_banorte_payment','')
    );
    $form['3dsecure'] = array(
        '#type'     => 'checkbox',
        '#title'    => '3D Secure',
        '#default_value'    => variable_get('3dsecure_banorte_payment','')
    );
    $form['merchantcity3d'] = array(
        '#type'=>'textfield',
        '#title' => t("Merchant City"),
        '#states' => array(
            
            // Hide the settings when the cancel notify checkbox is disabled.
            
            'visible' => array(
            
                ':input[name="3dsecure"]' => array('checked' => TRUE),
            ),
            'optional'=> array(
            
                ':input[name="3dsecure"]' => array('checked' => FALSE),
            ),
        ),
        '#default_value'=>variable_get('merchantcity3d_banorte_payment','')
    );
    $form['#submit'][]='payment_configuration_submit';
    return system_settings_form($form);
}
 
/**
 * payment_configuration_submit() this method define the variables required for
 *                                this payment module.
 */
function payment_configuration_submit($form, &$form_state){
    variable_set('mode_banorte_payment', $form_state['values']['mode']);
    variable_set('iscorrect_banorte_payment', $form_state['values']['iscorrect']);
    variable_set('isntcorrect_banorte_payment', $form_state['values']['isntcorrect']);
    variable_set('3dsecure_banorte_payment', $form_state['values']['3dsecure']);
    variable_set('merchantcity3d_banorte_payment', $form_state['values']['merchantcity3d']);
    variable_set('currency_banorte_payment', $form_state['values']['currency']);
    cache_clear_all();
}